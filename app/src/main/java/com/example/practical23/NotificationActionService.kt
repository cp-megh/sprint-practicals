package com.example.practical23

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NotificationActionService : BroadcastReceiver(){
    override fun onReceive(context: Context, intent: Intent) {
        context.sendBroadcast(
            Intent(context.getString(R.string.tracks_tracks))
                .putExtra(context.getString(R.string.action_name), intent.action)
        )
    }
}