package com.example.practical23

class Tracks {

    private var title: String? = null
    private var artist: String? = null
    private var image = 0
    private var song_raw = 0

    constructor(title: String?, artist: String?, image: Int, song_raw: Int) {
        this.title = title
        this.artist = artist
        this.image = image
        this.song_raw = song_raw
    }


    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getArtist(): String? {
        return artist
    }

    fun setArtist(artist: String?) {
        this.artist = artist
    }

    fun getImage(): Int {
        return image
    }

    fun setImage(image: Int) {
        this.image = image
    }

    fun getSong_raw(): Int {
        return song_raw
    }

    fun setSong_raw(song_raw: Int) {
        this.song_raw = song_raw
    }
}
